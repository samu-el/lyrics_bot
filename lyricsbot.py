#! python3
import sys
import time
import telepot
from telepot.loop import MessageLoop
from telepot.namedtuple import InlineQueryResultArticle, InputTextMessageContent
from googleapiclient.discovery import build
from pprint import pprint
import re
import urllib.request
from bs4 import BeautifulSoup
import datetime
import config

APITOKEN = config.LYRICS_API_TOKEN

my_api_key = config.GOOGLE_SEARCH_API
my_cse_id = config.GOOGLE_CSE_ID

bot = telepot.Bot(APITOKEN)
answerer = telepot.helper.Answerer(bot)


def handle(msg):
    try:
        log(msg)
        person_id = msg.get('chat').get('id')
        text = msg.get('text')
        if text.startswith('/'):
            bot.sendMessage(
                person_id, "Search lyrics by typing the artist's name and song title")
        else:
            title, lyrics = get_lyrics(text)
            bot.sendMessage(person_id, lyrics)
    except Exception as e:
        bot.sendMessage(
            person_id, "Something went wrong, please try again later")
        log("Handle: Error: "+str(e))


def on_inline_query(msg):
    log(msg)
    def compute():
        try:
            query_id, from_id, query_string = telepot.glance(
                msg, flavor='inline_query')
            # print('Inline Query:', query_id, from_id, query_string)
            if len(query_string) >= 6:
                title, lyrics = get_lyrics(query_string)
                articles = [InlineQueryResultArticle(
                    id='abc',
                    title=title,
                    input_message_content=InputTextMessageContent(
                        message_text=lyrics
                    )
                )]
                return articles
            else:
                articles = [InlineQueryResultArticle(
                    id='abc',
                    title='keep typing...',
                    input_message_content=InputTextMessageContent(
                        message_text="Search lyrics by typing the artist's name and song title"
                    )
                )]
                return articles
        except Exception as e:
            log(e)
            return None
    if compute != None:
        try:
            answerer.answer(msg, compute)
        except Exception as e:
            log(e)

def on_chosen_inline_result(msg):
    log(msg)
    result_id, from_id, query_string = telepot.glance(
        msg, flavor='chosen_inline_result')
    print('Chosen Inline Result:', result_id, from_id, query_string)


def google_search(search_term, api_key, cse_id, **kwargs):
    service = build("customsearch", "v1", developerKey=api_key)
    res = service.cse().list(q=search_term, cx=cse_id, **kwargs).execute()
    return res['items'][0]['link']


def get_lyrics(song):
    song = song.strip().split()
    uri = google_search(song, my_api_key, my_cse_id, num=1)
    try:
        req = urllib.request.Request(uri, headers={
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:58.0) Gecko/20100101 Firefox/58.0'})
        content = urllib.request.urlopen(req).read()
        soup = BeautifulSoup(content, 'html.parser')
        lyrics = str(soup)
        singer = lyrics.split("<h2><b>")[1]
        singer = singer.split("Lyrics</b></h2>")[0]
        title = lyrics.split("<b>")[2]
        title = title.split("</b>")[0]+"\n"
        up_partition = '<!-- Usage of azlyrics.com content by any third-party lyrics provider is prohibited by our licensing agreement. Sorry about that. -->'
        down_partition = '<!-- MxM banner -->'
        lyrics = singer + " - " + title + lyrics.split(up_partition)[1]
        lyrics = lyrics.split(down_partition)[0]
        lyrics = lyrics.replace('<br/>', '').replace('<br>', '').replace(
            '</br>', '').replace('<i>', '').replace('</i>', '').replace('</div>', '').strip()
        return singer + " " + title, lyrics
    except Exception as e:
        log("GetLyrics Error: "+str(e))
        return "Something went wrong, couldn't retrieve lyrics \n"


def log(msg):
    with open('log_lyrics.txt', 'a') as logfile:
        now = datetime.datetime.now()
        logfile.write(now.isoformat()+"\t"+str(msg)+"\n\n")


def main():
    MessageLoop(bot, {'chat': handle,
                      'inline_query': on_inline_query,
                      'chosen_inline_result': on_chosen_inline_result}).run_as_thread()

    print('Listening ...')
    log("******************************Server Started at" +
        datetime.datetime.now().isoformat() + "***************************")

    # Keep the program running.
    while 1:
        time.sleep(10)


if __name__ == '__main__':
    main()
